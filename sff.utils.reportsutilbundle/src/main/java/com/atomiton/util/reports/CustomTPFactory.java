package com.atomiton.util.reports;


import org.osgi.service.component.annotations.Component;

import com.atomiton.sff.api.nio.SffTpFunctionsSvc;

import oda.lm.ListMap;


@Component(service = SffTpFunctionsSvc.class)
public class CustomTPFactory implements SffTpFunctionsSvc {

	private ListMap info;
	
	private static final String WriteDataToExcelReport = "WriteToExcel";
	
	@Override
	public ListMap getInfo() {

		System.out.println("**************** Inside GetInfo of CustomTPFactory");

		info = ListMap.newInstance();
		info.put(WriteDataToExcelReport, new WriteDataToExcelReport());
		System.out.println("TPFactory Loaded : " + info.keySet());
		return info;
	}
}

