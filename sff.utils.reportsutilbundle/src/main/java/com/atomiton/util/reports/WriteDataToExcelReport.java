package com.atomiton.util.reports;

import static oda.common.TemplateProcessor.deepArg;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import oda.common.Str;
import oda.common.TemplateProcessor.IFunction;
import oda.lm.ListMap;

public class WriteDataToExcelReport implements IFunction {

	@Override
	public Object eval(ListMap cntx, List args) {
		String rootName, srcFileName = null, dstFileName = null;
		ListMap root = null;
		System.out.println(" Inside WriteDataToExcelReport.eval");
		/*Map dataTypeMAp = new HashMap<String, Integer>();
		dataTypeMAp.put("String", Cell.CELL_TYPE_STRING);
		dataTypeMAp.put("Numeric", Cell.CELL_TYPE_NUMERIC);
		dataTypeMAp.put("Formula", Cell.CELL_TYPE_FORMULA);
		dataTypeMAp.put("Blank", Cell.CELL_TYPE_BLANK);
		dataTypeMAp.put("Boolean", Cell.CELL_TYPE_BOOLEAN);*/
		Map formulaMap = new HashMap();
		if (args.size() >= 3) {
			rootName = (String) args.get(0);
			srcFileName = (String) args.get(1);
			dstFileName = (String) args.get(2);
			System.out.println(" rootName: " + rootName);
			System.out.println(" Report Template fileName: " + srcFileName);
			System.out.println(" Report fileName: " + dstFileName);

			try {
				Files.copy(Paths.get(srcFileName), Paths.get(dstFileName), StandardCopyOption.REPLACE_EXISTING);

				Workbook workbook;
				workbook = WorkbookFactory.create(new FileInputStream(dstFileName));
				Sheet sheet = workbook.getSheetAt(0);

				root = (ListMap) cntx.get(deepArg((rootName)));
				System.out.println(" root: " + root);
				List cellDataTypes = new ArrayList();
												
				if (root != null) {
					int totalCount = root.countKey("entry");
					System.out.println(" Entry count: " + totalCount);
					for (int i = 0; i < totalCount; i++) {
						String entry = (String) root.get("entry", i);
						//String[] values = entry.split(",");
						String[] values = Str.arraySplit(entry, ",");
						int cellid = 0;
						Row row = null;
						if(i>0){
							row = sheet.getRow(i);
							if(row==null){
								row = sheet.createRow(i);
								System.out.println("Row is null created new row");
							}
						}

						for (int j = 0; j < values.length; j++) {
							System.out.println(" Cell " + cellid + " Value is : " + values[j]);
							if(i>0){
								System.out.println("List : "+cellDataTypes);
								Cell cell;
								if("Numeric".equals(cellDataTypes.get(j)) && !values[j].isEmpty()){
									cell = row.createCell(cellid++);
									System.out.println("Inside Numeric value");
									cell.setCellValue(Double.parseDouble(values[j]));
									cell.setCellType(Cell.CELL_TYPE_NUMERIC);
								}else{
									if("Formula".equals(cellDataTypes.get(j))){
										System.out.println("Inside Formula value : ");
										cell = row.getCell(j);																	
										if(cell !=null )
										{
											String cellVal = cell.getStringCellValue();
											if(cellVal!= null && !"".equals(cellVal)){
												formulaMap.put(j, cellVal);
											}
											System.out.println("Formula  :: "+cellVal);
											System.out.println("Added formula in Map :: "+cellVal);
										}
										String myFormula = (String) formulaMap.get(j);
										if(myFormula != null && !"".equals(myFormula)){
											String formula = myFormula.replaceAll("RowNumber", (row.getRowNum()+1) +"");
											System.out.println("get formula from Map :: "+formula);
											String[] formulaFormat = formula.split("#");
											cell = row.createCell(cellid++);
											cell.setCellFormula(formulaFormat[0]);
											if(formulaFormat.length > 1){
												CellStyle cs = workbook.createCellStyle();
												CreationHelper createHelper = workbook.getCreationHelper();
												cs.setDataFormat(createHelper.createDataFormat().getFormat(formulaFormat[1]));
												cell.setCellStyle(cs);
											}											
										}
										
										System.out.println("Cell RowNumber ::"+cell.getRowIndex());
										//cellid++;
									
										//cell.setCellType(Cell.CELL_TYPE_NUMERIC);
										//cell.setCellFormula("IF(ISBLANK(I"+cell.getRowIndex()+1+"),\"\",(DATEVALUE(MID(I"+cell.getRowIndex()+1+",1,10))+TIMEVALUE(MID(I"+cell.getRowIndex()+1+",12,8))))");
									}else{
										cell = row.createCell(cellid++);
										cell.setCellValue(values[j]);
									}
								}							
								
							}else{
								cellDataTypes.add(values[j]);
							}
						}
						/*for (String val : values) {
							System.out.println(" Cell " + cellid + " Value is : " + val);
							if(i>0){

								Cell cell = row.createCell(cellid++);
								cell.setCellValue(val);
							}
							
						}*/
					}
				}
				workbook.setForceFormulaRecalculation(true);
				FileOutputStream fileOut = new FileOutputStream(new File(dstFileName));
				workbook.write(fileOut);
				workbook.close();
				fileOut.close();

			} catch (InvalidFormatException | IOException e) {
				e.printStackTrace();
				return e.getStackTrace();
			}
		}
		System.out.println("Report written successfully");
		return "true"+srcFileName+dstFileName;
	}
}
