package com.atomiton.util.reports;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class WriteDataToExcelReportTemp  {

	public static void main(String[] args) {
		System.out.println("WriteDataToExcelReportTemp.main()");
		String  srcFileName = "resources/temp1/spaces/sampleTemplate.xlsx", dstFileName = "resources/temp1/spaces/GenReports/sampleReport.xlsx";
		try {
			Files.copy(Paths.get(srcFileName), Paths.get(dstFileName), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}
